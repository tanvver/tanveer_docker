from flask import Flask, request

app = Flask(__name__)


@app.route('/')
def hello_world():
    res = "Hello from %s:%s to %s:%s" % (
        request.environ['REMOTE_ADDR'], request.environ['REMOTE_PORT'], request.environ['SERVER_NAME'],
        request.environ['SERVER_PORT'])
    return res


if __name__ == '__main__':
    app.run(host='0.0.0.0', port=8002)
